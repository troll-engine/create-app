# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/create-app/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">Creates a Troll Engine project</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/create-app/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/create-app/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/create-app/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/create-app/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/create" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/create" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/create" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/create" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm init @troll-engine
```

## Notes

  - The repository is named "create-app" instead of just "create"
  because "create" is a reserved name in Gitlab.
